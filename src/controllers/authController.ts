import { Router } from 'express';
import jsonwebtoken from 'jsonwebtoken';

import { AuthManager } from '../managers/authManager';

export const TOKEN_SECRET: string = process.env.APP_TOKEN_SECRET || '';
const authRouter = Router({ mergeParams: true });

authRouter.post('/', async(req, res, next) => {
	const result = await AuthManager.login(req.body, req.headers);
	if(result.error) res.send(result.error.message);
	
	let bearer = '';
	
	try {
		bearer = jsonwebtoken.sign({ ...result.data }, TOKEN_SECRET, {
			algorithm: 'none'
		});
	} catch (err) {
		console.log(err)
	}
	
	res.send(bearer);
});

export default authRouter;
