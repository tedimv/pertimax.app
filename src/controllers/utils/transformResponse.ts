import { Response } from 'express';

import { RepoActionResult } from '../../models';


export function shapeResponse<T>(controllerResponse: Response, managerPromise: Promise<RepoActionResult<T>>) {
	
	function fixBodyAndStatus(managerResult: RepoActionResult<T>) {
		controllerResponse.status(managerResult.status);
		switch (managerResult.status) {
			case 200:
			case 201:
			case 204:
				controllerResponse.json(managerResult.data);
				break;
			default:
				controllerResponse.send(managerResult.error?.message);
				break;
		}
	}
	
	managerPromise
		.then(fixBodyAndStatus)
		.catch(fixBodyAndStatus);
}
