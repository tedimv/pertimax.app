import { NextFunction, Request, Response } from 'express';

import { IOTS_types, IValidationSchema, ValidationErrorsList } from '../../models';
import { CreateUserC, BaseUserC } from '../../models/user';


export function bodyValidator(req: Request, res: Response, next: NextFunction) {
    const validationSchema: IValidationSchema = {
        '/users/create': CreateUserC,
        '/users/update': BaseUserC,
    };

    const validationCodec: IOTS_types = validationSchema[req.originalUrl];
    
    if (validationCodec) {
        const decoded = validationCodec.decode(req.body);
        if (decoded._tag === 'Left') {
            const validationErrors : ValidationErrorsList = [];
            decoded.left.forEach(error => {
                const {
                    key,
                    actual,
                    type
                } = error.context[2];

                validationErrors.push({
                    value: actual,
                    property: key,
                    expectedType: type.name
                });
            });
            
            res.status(400);
            res.json(validationErrors);
            return false;
        }
    }
    
    next();
}

