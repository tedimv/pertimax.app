import { Request, Router, Response } from 'express';

import { ProjectTaskDto } from '../models/projectTask';

import { projectTaskManager } from '../managers/projectTaskManager';
import { shapeResponse } from './utils/transformResponse';

export const projectTaskRouter = Router();

projectTaskRouter.get('/:id', async(req: Request, res: Response<ProjectTaskDto | string>, next) => {
	res.locals = await projectTaskManager.getProjectTask(req.params.id);
	next();
});

projectTaskRouter.post('/create', async(req: Request, res: Response<ProjectTaskDto | string>, next) => {
	res.locals = await projectTaskManager.createProjectTask(req.body);
	next();
});

projectTaskRouter.get('/', async(req: Request, res: Response<ProjectTaskDto[] | string>, next) => {
	res.locals = await projectTaskManager.getAllProjectsTasks();
	next();
});
