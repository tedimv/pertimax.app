import { Router } from 'express';
import moment from 'moment';

import { Notification } from '../models/notification';

const notificationsRouter = Router();

notificationsRouter.get('/notifications', (req, res) => {
    const hackNotification: Notification = {
        group: 'haks',
        event: {
            code: 'cyber',
            timestamp: moment.utc().format(),
            payload: null
        }
    }

    res.json(hackNotification);
});

export default notificationsRouter;