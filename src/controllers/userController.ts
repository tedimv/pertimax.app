import { Router, Response, Request } from 'express';

import { EntitiesList } from '../models';
import { CreateUser, UpdateUser, User } from '../models/user';

import { userRepo } from '../repositories/userRepository';

const usersRouter = Router({ mergeParams: true });

usersRouter.post('/create', async(req: Request<{}, {}, CreateUser>, res: Response<User | string>, next) => {
	res.locals = await userRepo.create(req.body);
	next();
});

usersRouter.get('/', async(req, res: Response<EntitiesList<User> | string>, next) => {
	const result = await userRepo.getAll();
	// @ts-ignore
	result.data?.entities.forEach(item => delete item.googleId);
	res.json(result.data);
	next();
});

usersRouter.get('/:id', async(req, res: Response<User | string>, next) => {
	res.locals = await userRepo.get({ _id: req.params.id });
	res.json(res.locals.data);
	next();
});

usersRouter.put('/update', async(req: Request<{}, {}, UpdateUser>, res: Response<UpdateUser | string>, next) => {
	res.locals = await userRepo.update(req.body);
	next();
});

usersRouter.delete('/delete/:id', async(req: Request<{ id: string }, {}, CreateUser>, res: Response<undefined | string>, next) => {
	res.locals = await userRepo.delete(req.params.id);
	next();
});


export default usersRouter;
