import { Router } from 'express';
import cors, { CorsOptions } from 'cors';

import usersRouter from './userController';
import notificationsRouter from './notification';
import { bodyValidator } from './utils/requestValidators';
import authRouter from './authController';

const mainRouter = Router();

const corsOptions: CorsOptions = {
	origin: 'http://localhost:3000',
}

mainRouter.use(cors(corsOptions));
mainRouter.use(bodyValidator);
mainRouter.use('/login', authRouter);
mainRouter.use('/users', usersRouter);
mainRouter.use('/notifications', notificationsRouter);

export default mainRouter;
