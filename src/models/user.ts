import * as iots from 'io-ts';

import { BaseEntityC } from './index';

export const BaseUserC = iots.intersection([
	BaseEntityC,
	iots.type({
		firstName: iots.string,
		familyName: iots.string,
		email: iots.string
	})
]);

export const CreateUserC = iots.type({
	googleId: iots.string,
	firstName: iots.string,
	familyName: iots.string,
	email: iots.string
})

export type User = iots.TypeOf<typeof CreateUserC>
export type CreateUser = iots.TypeOf<typeof CreateUserC>;
export type UpdateUser = iots.TypeOf<typeof BaseUserC>;
