import * as iots from 'io-ts';

import { EventC } from '.';

export const CNotification = iots.type({
    event: EventC,
    group: iots.string
}, 'Notification');

export type Notification = iots.TypeOf<typeof CNotification>;
