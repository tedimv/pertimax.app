import * as iots from 'io-ts';
import { Error as MongooseError, FilterQuery } from 'mongoose';


// Base entity
export const BaseEntityC = iots.type({
	_id: iots.union([ iots.string, iots.null ]),
});
export type TEntityPrimaryKey = iots.TypeOf<typeof BaseEntityC>

// BaseEntity with Name
export const BaseEntityWithNameC = iots.intersection([
	BaseEntityC,
	iots.type({ name: iots.string })
])
export type BaseEntityWithName = iots.TypeOf<typeof BaseEntityWithNameC>;

// Event
export const EventC = iots.type({
	timestamp: iots.string,
	code: iots.string,
	payload: iots.any
});
export type Event = iots.TypeOf<typeof EventC>;

// Contact information
export const ContactC = iots.type({
	phone: iots.string,
	email: iots.string,
	facebook: iots.string,
	linkedIn: iots.string
});
export type Contact = iots.TypeOf<typeof ContactC>;

// Action result
export const ActionResultC = iots.type({
	success: iots.boolean,
	error: iots.union([ iots.string, iots.null ])
});

export enum HttpMethods {
	create,
	get,
	put,
	delete
}

export interface RepoActionResult<T> {
	error: MongooseError | null
	data?: T,
	status: number,
	method: 'create' | 'get' | 'getAll' | 'put' | 'delete'
}

// Create/Update validation types
export type IOTS_types = iots.TypeC<any> | iots.IntersectionC<any> | iots.UnionC<any> | null;

export interface IValidationSchema {
	[apiUrl: string]: IOTS_types
};
export type ValidationError = {
	property: string,
	value: unknown,
	expectedType: string
};
export type ValidationErrorsList = ValidationError[];

// Generic Crud interface for all controllers
export interface ICrud<TEntity, TCreateEntity, TUpdateEntity extends TEntityPrimaryKey> {
	create: (entity: TCreateEntity) => Promise<RepoActionResult<TEntity>>,
	get: (query: FilterQuery<TEntity>, projection?: any) => Promise<RepoActionResult<TEntity>>,
	getAll: () => Promise<RepoActionResult<EntitiesList<TEntity>>>,
	update: (entity: TUpdateEntity) => Promise<RepoActionResult<TUpdateEntity>>,
	delete: (_id: string) => Promise<RepoActionResult<any>>,
}

// Entities list
export interface EntitiesList<T> {
	entities: T[],
	totalCount: number,
}

// Pagination query
export interface IPaginationQuery {
	skip: number,
	top: number,
}

export type QueryResult<T> = { _doc: T } | null
