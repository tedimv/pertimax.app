import iots from 'io-ts';
import { BaseEntityWithNameC } from '.';

// Base Dto
export const ProjectTaskStatusC = iots.keyof({
    0: 'New',
    1: 'Active',
    2: 'On Hold',
    3: 'Awaiting validation',
    4: 'Resolved',
})
export type ProjectTaskStatus = iots.TypeOf<typeof ProjectTaskStatusC>;

export const ProjectTaskC = iots.intersection([
    BaseEntityWithNameC,
    iots.type({
        descriptions: iots.string,
        status: ProjectTaskStatusC,
        owners: iots.array(BaseEntityWithNameC),
        viewers: iots.array(BaseEntityWithNameC),
        upstreamTasks: iots.array(iots.intersection([
            BaseEntityWithNameC,
            iots.type({ status: ProjectTaskStatusC })
        ])),
        downstreamTasks: iots.array(iots.intersection([
            BaseEntityWithNameC,
            iots.type({ status: ProjectTaskStatusC })
        ]))
    })
]);
export const CreateProjectTaskC = iots.type({
    name: iots.string,
    descriptions: iots.string,
    status: ProjectTaskStatusC,
    owners: iots.array(BaseEntityWithNameC),
    viewers: iots.array(BaseEntityWithNameC),
    upstreamTasks: iots.array(iots.intersection([
        BaseEntityWithNameC,
        iots.type({ status: ProjectTaskStatusC })
    ])),
    downstreamTasks: iots.array(iots.intersection([
        BaseEntityWithNameC,
        iots.type({ status: ProjectTaskStatusC })
    ]))
});
export type IProjectTask = iots.TypeOf<typeof ProjectTaskC>;
export type ProjectTaskDto = IProjectTask;
export type CreateProjectTask = iots.TypeOf<typeof CreateProjectTaskC>;
export type UpdateProjectTask = iots.TypeOf<typeof ProjectTaskC>;
