
import { CreateProjectTask, UpdateProjectTask, ProjectTaskDto } from '../models/projectTask';

import { projectTaskRepo } from '../repositories/projectTaskRepository';

export class projectTaskManager {
    
    static async createProjectTask(projectTaskDto: CreateProjectTask) {
        return await projectTaskRepo.create(projectTaskDto);
    }

    static async getProjectTask(_id: string) {
        return await projectTaskRepo.get(_id);
    }

    static async getAllProjectsTasks() {
        return await projectTaskRepo.getAll();
    }

    static async updateProjectTask(updateProjectTaskDto: UpdateProjectTask) {
        return await projectTaskRepo.update(updateProjectTaskDto);
    }

    static async deleteProjectTask(_id: string) {
        return await projectTaskRepo.delete(_id);
    }  

};