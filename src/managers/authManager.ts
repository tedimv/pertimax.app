import { OAuth2Client } from 'google-auth-library';
import { IncomingHttpHeaders } from 'http';
import jsonwebtoken from 'jsonwebtoken';

import { RepoActionResult } from '../models';
import { userRepo } from '../repositories/userRepository';
import { User } from '../models/user';
import { TOKEN_SECRET } from '../controllers/authController';

const clientId = '903532748185-s2tv2bj2dj8fr8dl4ftslkpikbf38ege.apps.googleusercontent.com';
const clientSecret = '0uXcdV5qx2JXzoUHogrOx-Pg';

export class AuthManager {
	static async login(data: any, headers: IncomingHttpHeaders): Promise<RepoActionResult<User>> {
		const result: RepoActionResult<any> = {
			data: null,
			error: null,
			method: 'create',
			status: 200
		}
		
		if (headers.authorization) {
			const token = headers.authorization.split(' ')[0];
			// @ts-ignore
			const isVerified = jsonwebtoken.verify(token, TOKEN_SECRET);
			if (isVerified) {
			
			}
		}
		
		try {
			const client = new OAuth2Client({ clientId, clientSecret });
			const ticket = await client.verifyIdToken({ idToken: data.token });
			const payload = ticket.getPayload();
			
			if(!payload?.email) {
				result.error = { name: 'Create user error.', message: 'Google account login not allowed.' };
				return result;
			}
			
			const userResult = await userRepo.get({ googleId: payload.sub }, { googleId: 0 });
			if(userResult.data) return userResult;
			
			const created = await userRepo.create({
				familyName: 'Beautiful',
				firstName: 'Stranger',
				email: payload.email,
				googleId: payload.sub
			});
			
			const extracted = { ...created.data };
			// @ts-ignore
			delete extracted.googleId;
			// @ts-ignore
			result.data = extracted
			return result;
		} catch (error) {
			return error;
		}
	}
}
