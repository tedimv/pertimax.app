import { Type} from 'io-ts';
import moment from 'moment';

import { Event } from '../models';


const errorCodes = {
    MODEL_VALIDATION: 'MODEL_VALIDATION'
}

export function validateModel<TC, T>(model: Type<TC>, data: T, callerFn: string): Event | T {
    const result = model.decode(data);
    if (result._tag === 'Left') {
        const validationError: Event = {
            timestamp: moment().format(),
            code: errorCodes.MODEL_VALIDATION,
            payload: { callerFn, statusCode: 400 }
        }
        return validationError;
    } else {
        return data;
    }
}