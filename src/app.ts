import express, { Express } from 'express';
import chalk from 'chalk';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';

import mainRouter from './controllers';
import './repositories';


dotenv.config();

const app: Express = express();
const port = 5000;


app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(mainRouter);
app.use((_, res) => {
    res.status(404);
    res.send();
})

app.listen(port, () => {
	console.log(chalk.green(`Listening on port ${port}.`));
});