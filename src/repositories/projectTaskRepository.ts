import { Document, model } from 'mongoose';
import { typedModel, createSchema, Type } from 'ts-mongoose';

import { IProjectTask as ProjectTask, ProjectTaskDto, CreateProjectTask, UpdateProjectTask } from '../models/projectTask';
import { createCrud } from '.';

const projectTaskSchema = createSchema({
    name: Type.string,
    description: Type.string,
    status: Type.number,
    owners: Type.array,
    viewers: Type.array,
    upstreamTasks: Type.array,
    downstreamTasks: Type.array
});

interface IProjectTaskModel extends Document, ProjectTask {
    _id: string
}

export const projectTaskModel = model<IProjectTaskModel>('ProjectModel', projectTaskSchema);
export const projectTaskRepo = createCrud<ProjectTask, CreateProjectTask, UpdateProjectTask>(projectTaskModel);