import { createSchema, typedModel } from 'ts-mongoose';

import { createCrud } from '.';
import { CreateUser, UpdateUser, User } from '../models/user';


export const userSchema = createSchema({
    firstName: String,
    familyName: String,
    email: String,
    googleId: String
});
export const userModel = typedModel('User', userSchema);
export const userRepo = createCrud<User, CreateUser, UpdateUser>(userModel);
