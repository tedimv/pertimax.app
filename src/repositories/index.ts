import chalk from 'chalk';
import { connect, FilterQuery, Model } from 'mongoose';

import { RepoActionResult, ICrud, EntitiesList, TEntityPrimaryKey, QueryResult } from '../models';


connect('mongodb://localhost:27017/pertimax', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
	useCreateIndex: true,
})
	.then(() => console.log(chalk.greenBright('Connected to DB.')))
	.catch(error => console.log(chalk.redBright('Could not connect to DB: ', error)));

export function createCrud<TEntity, TCreateEntity, TUpdateEntity extends TEntityPrimaryKey>(repoModel: Model<any>) {
	const crud: ICrud<TEntity, TCreateEntity, TUpdateEntity> = {
		create: async(entity: TCreateEntity) => {
			const result: RepoActionResult<TEntity> = {
				error: null,
				status: 200,
				method: 'create'
			};
			
			try {
				const found: QueryResult<TEntity> = await repoModel.create(entity);
				if(found) result.data = found._doc
				return result;
			} catch (error) {
				console.log(chalk.red('Could not CREATE entity: ', error));
				result.status = 500;
				result.error = error;
				throw result;
			}
		},
		get: async(query: FilterQuery<TEntity>, projection?: any) => {
			const result: RepoActionResult<TEntity> = {
				error: null,
				status: 200,
				method: 'get'
			};
			
			try {
				const found: QueryResult<TEntity> = await repoModel.findOne(query, projection);
				if(found) {
					result.data = found._doc;
				} else {
					result.status = 404;
					result.error = { name: 'Get single entity error.', message: 'Entity not found.' };
				}
				
				return result;
			} catch (error) {
				console.log(chalk.red('Could not GET entity: ', error));
				result.status = 500;
				result.error = error;
				throw result;
			}
		},
		getAll: async() => {
			const result: RepoActionResult<EntitiesList<TEntity>> = {
				error: null,
				status: 200,
				method: 'getAll',
			};
			
			try {
				const allEntities = await repoModel.find({});
				const entities: QueryResult<TEntity>[] = await repoModel.find({}).skip(0).select([]);
				const collected: TEntity[] = [];
				entities.forEach(item => item && collected.push(item._doc));
				
				result.data = {
					entities: collected,
					totalCount: allEntities.length
				}
				
				return result;
			} catch (error) {
				console.log(chalk.red('Could not GET_ALL entities: ', error));
				result.status = 500;
				result.error = error;
				throw result;
			}
		},
		update: async(entity: TUpdateEntity) => {
			const result: RepoActionResult<TUpdateEntity> = {
				error: null,
				status: 200,
				data: entity,
				method: 'put'
			};
			
			try {
				const updateResult = await repoModel.updateOne({ _id: entity._id }, entity);
				if(!updateResult.nModified) {
					result.status = 404;
					result.error = { name: 'Update single error.', message: 'Entity not found.' };
				}
				return result;
			} catch (error) {
				console.log(chalk.red('Could not UPDATE entity: ', error));
				result.status = 500;
				result.error = error;
				throw result;
			}
		},
		delete: async(_id: string) => {
			const result: RepoActionResult<null> = {
				error: null,
				status: 204,
				method: 'delete'
			};
			
			try {
				const deleteResult = await repoModel.deleteOne({ _id });
				if(!deleteResult.deletedCount) {
					result.status = 404;
					result.error = { name: 'Delete single entity error.', message: 'Entity not found.' };
				}
				return result;
			} catch (error) {
				console.log(chalk.red('Could not DELETE entity: ', error));
				result.status = 500;
				result.error = error;
				throw result;
			}
		}
	}
	return crud;
}
